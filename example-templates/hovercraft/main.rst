:title: %TITLE%
:css: css/main.css

.. |gitlogo| image:: images/logo_git.png
    :width: 64


=====

%TITLE%
====================

|gitlogo| Use of shortcut text

Tai Kedzierski
--------------

Title page content

.. image:: images/logo.png
    :width: 16
    :align: center


.. note::

    Presentation notes

    Image assumes that in the presentation source there is a sidecar :code:`images/` folder


Second slide
=============

Include inline code :code:`/bin/sh`

Code blocks
===========


.. code:: sh

    set -euo pipefail

Thanks
========

Links

<https://fosstodon.org/@taikedz>
