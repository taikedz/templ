# Jekyll + Gitlab site template

Use this template for a new jekyll site

## Gitlab CI control

A `.gitlab-ci.yml` is in this folder. Adjust it to your needs

## New site

(Assuming you've already added this template to your working directory)

Create a new site locally with

    jekyll new sitedir
    rsync -a sitedir/ ./
    rm -r sitedir
