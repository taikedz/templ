import argparse

import mypy.runner as runner

class Parser:
    """
    Use `argparse` to parse arguments
    Works in conjunction with `runner` to provide dry-run and show-commands modes.
    """

    def __init__(self):
        """
        Default initialisation. Provides --show-commands, --config and --dry-run out of the box.
        """

        self.arg_parser = argparse.ArgumentParser(allow_abbrev=False)

        self.addDefinition("--show-commands", {"help":"print external commands that are run", "action": "store_true"})
        self.addDefinition("--config", {"help":"config file path"})
        self.addDefinition("--dry-run", {"help":"do not execute commands, just list them", "action": "store_true"})


    def addDefinition(self, argname: str, argparams: dict):
        """ Add a single definition, with argparse-compatible keys.

        addDefinition("--my-arg", {"action": "store_true", "help", "some help"})
        """
        self.arg_parser.add_argument(argname, **argparams)


    def addDefinitions(self, paramlist: dict):
        """ Add a collection of definitions, mapping an argument name to argparse-compatible key-value paris.

        addDefinitions({
            "--my-arg": {"action": "store_true", "help": "help text"},
            "--my-arg2": {"action": "store_true", "help": "more help text"},
        })
        """
        for argname in paramlist:
            self.addDefinition(argname, paramlist[argname])


    def parse(self, effective_arguments: list):
        """ Parse an array of arguments.

        Returns a dict of flags found.
        """
        v = vars(self.arg_parser.parse_args(effective_arguments) )
        runner.setShowCommands(v["show_commands"])
        return v
