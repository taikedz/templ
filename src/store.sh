#%include std/abspath.sh
#%include std/cdiff.sh

$%function templ:store:save(sourcepath storename) {
    local storepath="$storename"

    templ:store:_get_store_path_to storepath

    local tmpstore="$storepath-tmp"
    templ:rm-if-e "$tmpstore"


    [[ ! -e "$storepath" ]] || askuser:confirm "Template '$storename' already exists (mode '$TEMPL_MODE') - overwrite?" || out:fail "Abort"

    if [[ "$TEMPL_MODE" = tar ]]; then
        tar cvzf "$tmpstore" "${T_EXCLUSIONS[@]:1}" -C "$sourcepath/" ./ || {
            templ:rm-if-e "$tmpstore"
            out:fail "Failed to create store."
        }

        mv "$tmpstore" "$storepath" || out:fail "Could not overwrite '$storepath' with '$tmpstore'"
    else
        (
            rsync -av --delete "${T_EXCLUSIONS[@]:1}" "$sourcepath/" "$tmpstore/" || out:fail "Failed to save $storename"
            templ:mv-if-e "$storepath" "$storepath-bak"
            mv "$tmpstore" "$storepath"
            templ:rm-if-e "$storepath-bak"
        ) || out:fail "Could not create $storename"
    fi
}

$%function templ:rm-if-e(path) {
    [[ ! -e "$path" ]] || rm -r "$path"
}

$%function templ:mv-if-e(from to) {
    [[ ! -e "$from" ]] || mv "$from" "$to"
}

$%function templ:store:diff(storename sourcepath) {
    local storepath="$storename"

    if [[ "$TEMPL_MODE" = tar ]]; then
        out:fail "Cannot diff in 'tar' mode"
    fi

    templ:store:_get_store_path_to storepath

    [[ -d "$storepath" ]] || out:fail "No such store '$storename'"

    diff -u "$storepath" "$sourcepath" | cdiff:colorize
}

$%function templ:store:apply(storename destpath) {
    local storepath="$storename"

    templ:store:_get_store_path_to storepath

    [[ -e "$storepath" ]] || out:fail "No such template '$storename'"
    [[ -d "$destpath" ]] || mkdir -p "$destpath"

    (
    if [[ "$TEMPL_MODE" = tar ]]; then
        tar xvzf "$storepath" -C "$destpath/"
    else
        rsync -avu "$storepath/" "$destpath/"
    fi
    ) || out:fail "Failed to apply $storename"
}

$%function templ:store:_get_store_path_to(*p_targetname) {
    local suffix

    if [[ "$TEMPL_MODE" = tar ]]; then
        suffix=".tar.gz"
    fi
    p_targetname="$(cd "$TEMPL_STORE"; abspath:path "$p_targetname${suffix:-}")"
}

templ:store:ensure() {
    TEMPL_STORE="${TEMPL_STORE:-$HOME/.local/templ_store}"
    
    [[ -d "$TEMPL_STORE" ]] || mkdir -p "$TEMPL_STORE" || out:fail "Could not create store '$TEMPL_STORE'"
}

templ:store:list() {
    (ls "$TEMPL_STORE"|sed -r 's/\.tar\.gz/ (tarball)/'| grep -E '^') || out:info "No stores."
}

$%function templ:store:show(template_name) {
    templ:store:_get_store_path_to template_name
    if [[ -n "$*" ]]; then
        templ:store:show_files "$template_name" "$@"
        return
    fi

    if [[ "$TEMPL_MODE" = tar ]]; then
        tar tzf "$template_name"
    else
        (cd "$template_name"
        find .
        )
    fi
}

$%function templ:store:show_files(template_path) {
    local subfile
    if [[ "$TEMPL_MODE" = tar ]]; then
        for subfile in "$@"; do
            out:info "$subfile"
            tar xzf "$template_path" -O "$subfile"
        done
    else
        for subfile in "$@"; do
            out:info "$subfile"
            cat "$template_name/$subfile"
        done
    fi
}

$%function templ:store:addfile(template_name) {
    local file
    local storepath="$template_name"
    templ:store:_get_store_path_to storepath

    if [[ "$TEMPL_MODE" = tar ]]; then
        tar --update -f "$storepath" "$@"
    else
        cp -r "$@" "$storepath/"
    fi
}

$%function templ:store:rm(template_name) {
    local template_path="$template_name"
    templ:store:_get_store_path_to template_path

    [[ -e "$template_path" ]] || out:fail "No such template '$template_name' (mode: '$TEMPL_MODE')"

    local result="$(askuser:ask "Remove '$template_name' ? Type '$template_name' > ")"

    if [[ "$result" = "$template_name" ]]; then
        out:info "Removing $template_name"

        rm -r "$template_path"
    else
        out:fail "Abort"
    fi
}

$%function templ:store:check_alt(alt_store) {
    [[ -n "$alt_store" ]] || out:fail "No alternate store specified. Use '--alt ALTSTORE' to specify it."
    [[ -d "$alt_store" ]] || out:fail "Alternate store '$alt_store' does not exist."
}

$%function templ:store:import(alt_store) {
    templ:store:check_alt "$alt_store"
    templ:store:transvase "$alt_store" "$TEMPL_STORE"
}

$%function templ:store:export(alt_store) {
    templ:store:check_alt "$alt_store"
    templ:store:transvase "$TEMPL_STORE" "$alt_store"
}

$%function templ:store:transvase(from_store to_store) {
    local store_item
    out:info "Copying from '$from_store' to '$to_store'. Are you sure?"
    out:warn "Same-name stores will be overwritten!"
    askuser:confirm "Proceed?" || out:fail "abort"

    while read store_item; do
        out:info "$from_store/$store_item --> $to_store/$store_item"

        if [[ -d "$from_store/$store_item" ]]; then
            rsync -a --delete "$from_store/$store_item/" "$to_store/$store_item/"
        elif [[ -f "$from_store/$store_item" ]]; then
            cp  "$from_store/$store_item" "$to_store/$store_item"
        else
            out:error "Not a file or directory '$store_item'"
        fi
    done < <(ls "$from_store")
}
