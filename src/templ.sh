#!/usr/bin/env bbrun

### templ : Store and apply template folder content Usage:help
#
# A tool to turn a directory into a template, or copy a template's contents into a directory.
#
#
# COMMANDS:
#
#   templ save TEMPLNAME [DIRECTORY] [OPTIONS]
#
# Save DIRECTORY as a template
# DIRECTORY defaults to "./" when not specified.
# When saving a directory's contents over an existing template, removes old template files not found in the new target.
#
#   templ apply TEMPLNAME [DIRECTORY] [OPTIONS]
#
# Apply a template to the specified directory.
# DIRECTORY defaults to "./" when not specified.
# When applying a template's contents to a directory, only updates files that are older by date, and does not delete other files with matching path.
#
#   templ add TEMPLNAME [OPTIONS ...] [FILES ...]
#
# Individually add files to an existing template
#
#   templ diff TEMPLNAME [DIRECTORY] [OPTIONS]
#
# See what changes exist in your DIRECTORY compared to the template. Only works on non-tar templates.
#
#   templ list [OPTIONS]
#
# List saved templates.
#
#   templ show TEMPLNAME [OPTIONS] [FILES]
#
# List files in a template. If FILES are specified, dump their contents instead.
#
#   templ rm TEMPLNAME [OPTIONS]
#
# Remove the named template.
#
#   templ import --alt ALTSTORE
#   templ export --alt ALTSTORE
#
# Import templates from an alternate store to your main store; export from main store to the alt store.
#
#
#
# OPTIONS:
#
# `-x,--exclude PATTERN` - where PATTERN is a comma-separated list of glob(3) patterns to exclude, e.g. `-x '.git/,bin/*'`
#
# `-f,--exclude-from FILE` - where FILE is a file containing globs to exclude. e.g.
#
#   *.swp
#   src/__pycache__
#   *.pyc
#
# `--standard-exclusions,--se` - Exclude some common things: `.git/` directory, `*.swp` files, `*~`, `*.pyc`, `__pycache__/` folders
#
# `--store,-s STOREPATH` - Path to the template store to use (default is $HOME/.local/templ_store)
#
# `--tar` - Create tarballs instead of plain folders ; prefer this mode if you are using a file sync solution for your store.
#
#
#
# ENVIRONMENT VARIABLES
#
# You can export these environment variables to alter the behaviour of templ without switches.
#
# TEMPL_MODE - if "tar", uses tar, else uses rsync. Same as using `--tar`
#
# TEMPL_STORE - the path to where templates should be stored. Same as using `--store STOREPATH`
#
###/doc

#%include std/safe.sh
#%include std/out.sh
#%include std/autohelp.sh
#%include std/askuser.sh
#%include std/args.sh

#%include store.sh

$%function templ:check(template_name) {
    [[ "$template_name" =~ ^[a-zA-Z0-9_-]+$ ]] || out:fail "Not a valid template name: '$template_name'. Please use only letters, numbers, '_' or '-'"
}

### argument parsing Usage:api
#
# A few global variables get set here, and can be used throughout the script
#
# TEMPL_MODE - the mode in which to operate (rsync/tar), can be inherited from the environment
# TEMPL_STORE - the store folder to use
# TEMPL_ALT_STORE - the alt store to use, for import/export commands
# T_EXCLUSIONS - the exclusion tokens, adapted for each of rsync and tar modes (some flags are incompatible between tools)
#
# T_template_name - the name of the template to operate on - always required
# T_folder - the folder to operate on for save/apply
# T_POSARGS_NOTEMPLATE - positional arguments, without the action and template name tokens.
#   In case of save/apply actions, will include the operation folder as first item
#
###/doc
$%function templ:parseargs() {
    local exclusions_file exclusions_pat standard_exclusions tarmode
    local argdef=(
        s:exclusions_file:--exclude-from,-f
        s:exclusions_pat:--exclude,-x
        b:standard_exclusions:--standard-exclusions,--se
        s:TEMPL_STORE:--store,-s
        b:tarmode:--tar
        s:TEMPL_ALT_STORE:--alt
    )

    local positionals

    args:parse argdef positionals "$@"

    TEMPL_MODE="${TEMPL_MODE:-rsync}"
    if [[ "${tarmode:-}" = true ]]; then
        TEMPL_MODE=tar
    fi

    T_template_name="${positionals[0]:-}"
    T_folder="${positionals[1]:-}"

    T_POSARGS_NOTEMPLATE=("${positionals[@]:1}")

    if [[ "$TEMPL_MODE" = tar ]]; then
        T_EXCLUSIONS=(:)
    else
        T_EXCLUSIONS=(: --delete-excluded)
    fi

    if [[ -n "${exclusions_file:-}" ]]; then
        [[ -f "$exclusions_file" ]] || out:fail "'$exclusions_file' is not a file."
        T_EXCLUSIONS+=(--exclude-from="$exclusions_file")
    fi

    [[ -z "${exclusions_pat:-}" ]] || T_EXCLUSIONS+=(--exclude="$exclusions_pat")

    if [[ "${standard_exclusions:-}" = true ]]; then
        T_EXCLUSIONS+=(--exclude='.git/,*.swp,*~,*.pyc,__pycache__/')
    fi
}

$%function templ:main(action) {
    templ:parseargs "$@"

    templ:store:ensure

    case "$action" in
    list|ls)
        templ:store:list
        exit
        ;;
    import)
        templ:store:import "${TEMPL_ALT_STORE:-}"
        exit
        ;;
    export)
        templ:store:export "${TEMPL_ALT_STORE:-}"
        exit
        ;;
    esac

    templ:check "$T_template_name"

    case "$action" in
    save)
        templ:store:save "${T_folder:-./}" "$T_template_name"
        ;;
    apply)
        templ:store:apply "$T_template_name" "${T_folder:-./}"
        ;;
    diff)
        templ:store:diff "$T_template_name" "${T_folder:-./}"
        ;;
    show)
        templ:store:show "$T_template_name" "${T_POSARGS_NOTEMPLATE[@]}"
        ;;
    add)
        templ:store:addfile "$T_template_name" "${T_POSARGS_NOTEMPLATE[@]}"
        ;;
    rm)
        templ:store:rm "$T_template_name"
        ;;
    *)
        out:fail "Unknown action '$action'"
        ;;
    esac
}

autohelp:check-or-null "$@"
templ:main "$@"
