
templ : Store and apply template folder content
=======
 
A tool to turn a directory into a template, or copy a template's contents into a directory.
 
 
COMMANDS:
 
    templ save TEMPLNAME [DIRECTORY] [OPTIONS]
 
Save DIRECTORY as a template
DIRECTORY defaults to "./" when not specified.
When saving a directory's contents over an existing template, removes old template files not found in the new target.
 
    templ apply TEMPLNAME [DIRECTORY] [OPTIONS]
 
Apply a template to the specified directory.
DIRECTORY defaults to "./" when not specified.
When applying a template's contents to a directory, only updates files that are older by date, and does not delete other files with matching path.
 
    templ add TEMPLNAME [OPTIONS ...] [FILES ...]
 
Individually add files to an existing template
 
    templ diff TEMPLNAME [DIRECTORY] [OPTIONS]
 
See what changes exist in your DIRECTORY compared to the template. Only works on non-tar templates.
 
    templ list [OPTIONS]
 
List saved templates.
 
    templ show TEMPLNAME [OPTIONS] [FILES]
 
List files in a template. If FILES are specified, dump their contents instead.
 
    templ rm TEMPLNAME [OPTIONS]
 
Remove the named template.
 
    templ import --alt ALTSTORE
    templ export --alt ALTSTORE
 
Import templates from an alternate store to your main store; export from main store to the alt store.
 
 
 
OPTIONS:
 
`-x,--exclude PATTERN` - where PATTERN is a comma-separated list of glob(3) patterns to exclude, e.g. `-x '.git/,bin/*'`
 
`-f,--exclude-from FILE` - where FILE is a file containing globs to exclude. e.g.
 
    *.swp
    src/__pycache__
    *.pyc
 
`--standard-exclusions,--se` - Exclude some common things: `.git/` directory, `*.swp` files, `*~`, `*.pyc`, `__pycache__/` folders
 
`--store,-s STOREPATH` - Path to the template store to use (default is $HOME/.local/templ_store)
 
`--tar` - Create tarballs instead of plain folders ; prefer this mode if you are using a file sync solution for your store.
 
 
 
ENVIRONMENT VARIABLES
 
You can export these environment variables to alter the behaviour of templ without switches.
 
TEMPL_MODE - if "tar", uses tar, else uses rsync. Same as using `--tar`
 
TEMPL_STORE - the path to where templates should be stored. Same as using `--store STOREPATH`
 
