#!/usr/bin/env bash

cd "$(dirname "$0")"

bindir="$HOME/.local/bin"

[[ "$UID" != 0 ]] || bindir="/usr/local/bin"

mkdir -p "$bindir"

cp "bin/templ" "$bindir"

if [[ "$*" =~ --examples ]]; then
    bin/templ import --alt example-templates
fi

echo "Installed to $bindir"
