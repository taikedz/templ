# templ : Folder templates

Use `templ` to turn a folder into a template to apply later.

```sh
# Create a template based on an existing folder
templ save my-template target-folder/

# Apply the template contents to the folder
# If a file exists and is newer than the template folder, it is NOT updated
templ apply my-template other-folder/
```

See [the help file](help.md) for more info, or run `templ --help`

## Install

```sh
./install.sh [--examples]
```

Install `templ` and initialize the template store with useful examples.

## Examples

### Boilerplate code for Jekyll on GitLab Pages

If you are starting a new Jekyll site on GitLab Pages, you need a certain amount of boiler to get started. To get that boilerplate into the local directory, run

```sh
templ apply jekyll-gitlab
```

### Apply a license

The `license` example template privdes the standard text for a number of popular open source licenses. If you want to set a license file in your project, you can run

```sh
templ show license
```

which will show you available licenses. You can then extract one by name and write it to file

```sh
templ show license mit > LICENSE.txt
```
